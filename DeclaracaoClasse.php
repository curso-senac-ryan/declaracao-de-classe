<?php

abstract class Forma
{
    public $tipoDeForma = 'Forma Abstrata';

    public function imprimeForma()
    {
        echo $this->tipoDeForma . ' com Área de: ' . $this-> calculaArea();
    }

    abstract public function calculaArea();
}


class Quadrado extends Forma
{
    public $lado;

    public function __construct(float $varLado)
    {
        $this-> tipoDeForma = 'Quadrado';
        $this-> lado = $varLado;
    }

    public function calculaArea()
    {
        return $this-> lado * $this-> lado;
    }
}


class Retangulo extends Forma
{
    public $base;
    public $altura;

    public function __construct($base, $altura)
    {
        $this-> tipoDeForma = "Retangulo";
        $this-> base = $base;
        $this-> altura = $altura;
    }

    public function calculaArea()
    {
        return $this-> base * $this-> altura;
    }
}


class Triangulo extends Forma
{
    public $comprimentoBase;
    public $altura;

    public function __construct($comprimentoBase, $altura)
    {
        $this-> comprimentoBase = "Triangulo";
        $this-> altura = $altura;
    }

    public function calculaArea()
    {
        return $this-> comprimentoBase * $this-> altura;
    }
}

$obj = new Quadrado(5);
$obj-> imprimeForma();

echo '<br>';

$obj1 = new Quadrado(100);
$obj1-> imprimeForma();

echo '<br>';

$obj2 = new Retangulo(5, 10);
$obj2 -> imprimeForma();

echo '<br>';

$obj3 = new Triangulo(6, 10);
$obj-> imprimeForma();
