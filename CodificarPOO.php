<?php

abstract class Conta 
{
    private $tipoDaConta;
    public function getTipoDaConta(){
        return $this-> tipoConta;
    }
    public function setTipoDaConta( String $TipoDaConta ){
        $this-> tipoDaConta = $TipoDaConta;
    }

    public $agencia;
    public $conta;
    protected $saldo;


    public function deposito(float $valor)
    {
        if ($valor> 0) {
        //$this-> saldo = $this-> saldo + $valor;
        $this-> saldo += $valor;
        echo "Depósito efetuado com sucesso!!! <br>";
    }
    else
    {
        echo "Valor do depósito invalido!!! <br>";
    }
}

    public function saque(float $valor)
    {
        if ($this-> saldo >= $valor) {
            $this-> saldo = $this-> saldo - $valor;
            echo "Saque realizado com sucesso" . "<br>"; 

        } else {
            echo "Saldo Insuficiente!!!" . "<br>";
        }
    }

    abstract public function calculaSaldo();

    public function imprimeExtrato()
    {
        echo "Conta " . $this -> tipoDaConta .  " Agencia: " . $this->agencia . " Conta: " . $this-> conta . " Saldo: " . $this-> calculaSaldo();
    }
}

class Poupanca extends Conta
{
    public $reajuste;

    public function __construct(string $agencia, string $conta, float $reajuste)
    {
        $this-> setTipoDaConta('Poupança');
        $this-> agencia = $agencia;
        $this-> conta = $conta;
        $this-> reajuste = $reajuste;
    }

    public function calculaSaldo()
    {
        return $this-> saldo + ($this-> saldo * $this->reajuste / 100);
    }
}

class Especial extends Conta
{
    public $saldoEspecial;
    public function __construct(string $agencia, string $conta, float $saldoEspecial)
    {
        $this-> setTipoDaConta('Especial');
        $this-> agencia = $agencia;
        $this-> conta = $conta;
        $this-> saldoEspecial = $saldoEspecial;
    }

    public function calculaSaldo()
    {
        return $this-> saldo + $this-> saldoEspecial;
    }
}


$ctaPoupanca = new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoupanca-> saldo -1500; -- Nao pode acessar atributo protegido
$ctaPoupanca-> deposito(1500);
$ctaPoupanca-> saque(3000);
$ctaPoupanca-> imprimeExtrato();

echo '<br>';

$ctaEspecial = new Especial('0055-2', '75588-42', 2300);
$ctaEspecial-> deposito(1500);
$ctaEspecial-> imprimeExtrato();

